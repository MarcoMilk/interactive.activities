﻿namespace UiPathTeam.Interactive.Activities.Properties
{
    public enum WindowPositions
    {
        [LocalizedDisplayName(nameof(Resources.TopLeft))]
        [LocalizedDescription(nameof(Resources.TopLeft))]
        TopLeft,
        [LocalizedDisplayName(nameof(Resources.TopRight))]
        [LocalizedDescription(nameof(Resources.TopRight))]
        TopRight,
        [LocalizedDisplayName(nameof(Resources.BottomLeft))]
        [LocalizedDescription(nameof(Resources.BottomLeft))]
        BottomLeft,
        [LocalizedDescription(nameof(Resources.BottomRight))]
        [LocalizedDisplayName(nameof(Resources.BottomRight))]
        BottomRight,
        [LocalizedDescription(nameof(Resources.Center))]
        [LocalizedDisplayName(nameof(Resources.Center))]
        Center,
        [LocalizedDescription(nameof(Resources.TopCenter))]
        [LocalizedDisplayName(nameof(Resources.TopCenter))]
        TopCenter,
        [LocalizedDescription(nameof(Resources.BottomCenter))]
        [LocalizedDisplayName(nameof(Resources.BottomCenter))]
        BottomCenter
    }
}