﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using UiPathTeam.Interactive.Activities.Properties;
using System.Linq;

namespace UiPathTeam.Interactive.Activities.Forms
{
    public partial class InfoPanelForm : GenericForm
    {
        public InfoPanelForm()
        {
            InitializeComponent();
        }

        private const int lblMarginTop = 10;
        private const int lblMarginBottom = 30;

        private const int barMarginTop = 30;
        private const int barMarginBottom = 30;

        private const int rowHeight = 40;
        public string title = "Information Panel";
        public int? value;
        public bool hasButtons = true;
        public string pauseText = Resources.ButtonPauseText;
        public string unpauseText = Resources.ButtonUnPauseText;
        public string closeText = Resources.ButtonCloseText;

        public Dictionary<string, string> msgs;

        private int lblRow;
        private int barRow;

        public static Random rnd = new Random();

        private bool shouldPause = false;
        private bool shouldClose = false;

        public bool ShouldPause { get => shouldPause; set {
                shouldPause = value;
                var btn = this.getBtnPause();
                var bar = this.getProgressBar();
                if (btn != null && btn.GetType() == typeof(System.Windows.Forms.Button))
                {
                    btn.Text = this.ShouldPause ? unpauseText : pauseText;
                    btn.Image = !this.ShouldPause ? Resources.Pause : Resources.Play;
                }
                if (bar != null && bar.GetType() == typeof(System.Windows.Forms.ProgressBar))
                {
                    var color = this.ShouldPause ? ProgressColors.Yellow : ProgressColors.Green;
                    NativeMethods.SendMessage((int)bar.Handle, NativeMethods.PBM_SETBARCOLOR, (int)color, 0);
                }
            }
        }

        public bool ShouldClose
        {
            get => shouldClose; set
            {
                shouldPause = value;
                var bar = this.getProgressBar();
                if (bar != null && bar.GetType() == typeof(System.Windows.Forms.ProgressBar))
                {
                    var color = this.ShouldPause ? ProgressColors.Yellow : ProgressColors.Green;
                    color = this.ShouldClose ? ProgressColors.Yellow : color;
                    NativeMethods.SendMessage((int)bar.Handle, NativeMethods.PBM_SETBARCOLOR, (int)color, 0);
                }
            }
        }

        public event EventHandler PauseRequested;

        protected virtual void OnPauseRequested(EventArgs e)
        {
            EventHandler handler = PauseRequested;
            handler?.Invoke(this, e);
        }

        internal static Label GetNewLabel(string text, ContentAlignment align = ContentAlignment.MiddleLeft)
        {
            return new Label()
            {
                Text = text,
                Dock = DockStyle.Fill,
                TextAlign = align,
                //BackColor = Color.FromArgb(rnd.Next(100, 255), rnd.Next(100, 255), rnd.Next(100, 255)),
                //ForeColor = Color.Black,
            };
        }

        public void initTable()
        {
            this.SuspendLayout();
            this.table.Controls.Clear();
            this.table.RowStyles.Clear();
            this.table.ColumnStyles.Clear();

            int index = 0;

            //Set title row
            if (!String.IsNullOrEmpty(title))
            {
                var g = Graphics.FromHwnd(IntPtr.Zero);
                var lblTitle = GetNewLabel(title, ContentAlignment.MiddleCenter);
                var lblFont = new Font(lblTitle.Font.Name, 20F);
                var size = g.MeasureString(title, lblFont).ToSize();
                lblTitle.Font = lblFont;
                lblTitle.Margin = new Padding(0, lblMarginTop, 0, lblMarginBottom);
                lblTitle.AutoSize = true;
                lblTitle.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;
                this.table.RowStyles.Add(new RowStyle(SizeType.Absolute, size.Height + lblMarginTop + lblMarginBottom));
                this.table.Controls.Add(lblTitle, 0, index);
                this.table.SetColumnSpan(lblTitle, 2);
                this.lblRow = index;
                this.table.RowCount = 1;
                index++;
            }

            //if there are messages to display
            if (msgs != null && msgs.Count > 0)
            {
                this.table.ColumnCount = 2;
                this.table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 30F));
                this.table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 70F));
                foreach (var msg in msgs)
                {
                    this.table.RowStyles.Add(new RowStyle(SizeType.Absolute, rowHeight));
                    this.table.Controls.Add(GetNewLabel(msg.Key), 0, index);
                    this.table.Controls.Add(GetNewLabel(msg.Value), 1, index);
                    index++;
                    this.table.RowCount++;
                }
            }

            //if there is value add progress bar to the table
            if (this.value.HasValue)
            {
                var barProgress = new System.Windows.Forms.ProgressBar
                {
                    Value = value.Value,
                    Width = table.Width,
                    Height = rowHeight,
                    Margin = new Padding(10, barMarginTop, 10, barMarginBottom),
                    Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top
                };
                this.table.RowStyles.Add(new RowStyle(SizeType.Absolute, rowHeight + barMarginTop + barMarginBottom));
                this.table.Controls.Add(barProgress, 0, index);
                this.table.SetColumnSpan(barProgress, 2);
                this.barRow = index;
                index++;
                this.table.RowCount++;
            }

            //if there are buttons to display show flowLayoutPanel
            if (this.hasButtons)
            {
                var flow = flowLayoutPanel1;
                var btnPause = new Button();
                btnPause.Click += new System.EventHandler(btnPauseClick);
                btnPause.TextImageRelation = TextImageRelation.ImageBeforeText;
                btnPause.TextAlign = ContentAlignment.MiddleCenter;
                btnPause.ImageAlign = ContentAlignment.MiddleRight;
                btnPause.Width = flow.ClientSize.Width / 2 - 20;
                btnPause.Height = rowHeight;

                var btnClose = new Button();
                btnClose.Text = this.closeText;
                btnClose.Image = Resources.cancel_16;
                btnClose.Click += new System.EventHandler(btnCloseClick);
                btnClose.TextImageRelation = TextImageRelation.ImageBeforeText;
                btnClose.TextAlign = ContentAlignment.MiddleCenter;
                btnClose.ImageAlign = ContentAlignment.MiddleRight;
                btnClose.Width = flow.ClientSize.Width / 2 - 20;
                btnClose.Height = rowHeight;

                flow.Width = this.Width;
                flow.Height =  rowHeight + 10;
                flow.Controls.Add(btnPause);
                flow.Controls.Add(btnClose);
                flow.WrapContents = true;

                this.Height = table.GetRowHeights().Sum() + table.Margin.Vertical + flowLayoutPanel1.Height + flowLayoutPanel1.Margin.Vertical;

            }
            else {
                flowLayoutPanel1.Hide();
                this.Height = table.Height;
                this.Height = table.GetRowHeights().Sum() + table.Margin.Vertical;
            }

            this.SetPosition(this.position);
            this.ResumeLayout();
        }

        internal static void HideWindow(int handle)
        {
            NativeMethods.sendWindowsStringMessage(handle, NativeMethods.WINDOW_HIDE, "");
        }

        public void btnPauseClick(object sender, EventArgs e)
        {
            this.ShouldPause = !this.ShouldPause;
            var args = new InfoPanelEventArgs(this.ShouldPause);
            EventHandler handler = PauseRequested;
            handler?.Invoke(this, args);
        }
        public void btnCloseClick(object sender, EventArgs e)
        {
            this.ShouldClose = true;
            this.Close();
        }

        public void updateTable()
        {
            if (!String.IsNullOrEmpty(this.title))
            {
                var lbl = (Label)this.table.GetControlFromPosition(0, lblRow);
                if (lbl.Text != this.title) lbl.Text = this.title;
            }

            int index = lblRow + 1;
            if (msgs != null && msgs.Count > 0)
            {
                foreach (var msg in msgs)
                {
                    var keyLbl = (Label)this.table.GetControlFromPosition(0, index);
                    keyLbl.Text = msg.Key;
                    var valueLbl = (Label)this.table.GetControlFromPosition(1, index);
                    valueLbl.Text = msg.Value;
                    index++;
                }
            }

            if (this.value.HasValue)
            {
                var ctrl = this.table.GetControlFromPosition(0, this.barRow);
                if (ctrl.GetType() == typeof(System.Windows.Forms.ProgressBar))
                {
                    var bar = (System.Windows.Forms.ProgressBar)ctrl;
                    bar.Value = this.value.Value;
                    bar.PerformStep();
                }
            }
        }

        public Button getBtnPause()
        {
            if (this.hasButtons)
            {
                /*var ctrl = this.table.GetControlFromPosition(0, this.btnRow);
                if (ctrl != null)
                {
                    if (ctrl.Controls.Count > 0) return (Button)ctrl.Controls[0];
                }
                */
                var ctrl = flowLayoutPanel1;
                if (ctrl.Controls.Count > 0) return (Button)ctrl.Controls[0];
            }
            return null;
        }

        public System.Windows.Forms.ProgressBar getProgressBar()
        {
            if (this.value.HasValue)
            {
                var ctrl = this.table.GetControlFromPosition(0, this.barRow);
                if (ctrl != null)
                {
                    return (System.Windows.Forms.ProgressBar)ctrl;
                }
            }
            return null;
        }

        public static void UpdateWindow(int handle, string title, int value, Dictionary<string, string> msgs, string pauseText, string unpauseText, string closeText, bool shouldPause)
        {
            msgs["__title"] = title;
            msgs["__value"] = value.ToString();
            msgs["__pauseText"] = pauseText;
            msgs["__unpauseText"] = unpauseText;
            msgs["__closeText"] = closeText;
            msgs["__pause"] = shouldPause?"1":"0";
            NativeMethods.sendWindowsStringMessage(handle, NativeMethods.TEXT_UPDATE, JsonConvert.SerializeObject(msgs));
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case NativeMethods.WM_COPYDATA:
                    NativeMethods.COPYDATASTRUCT mystr = new NativeMethods.COPYDATASTRUCT();
                    Type mytype = mystr.GetType();
                    mystr = (NativeMethods.COPYDATASTRUCT)m.GetLParam(mytype);

                    switch ((int)m.WParam)
                    {
                        case NativeMethods.TEXT_UPDATE:
                            this.SetData(mystr.lpData);
                            break;
                    }
                    break;
            }

            base.WndProc(ref m);
        }

        internal bool SetData(string json)
        {
            try
            {
                msgs = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                this.title = msgs["__title"];
                this.ShouldPause = (msgs["__pause"] == "1");
                this.value = Int32.Parse(msgs["__value"]);
                this.pauseText = msgs["__pauseText"];
                this.unpauseText = msgs["__unpauseText"];
                this.closeText = msgs["__closeText"];
                msgs.Remove("__title");
                msgs.Remove("__value");
                msgs.Remove("__pause");
                msgs.Remove("__pauseText");
                msgs.Remove("__unpauseText");
                msgs.Remove("__closeText");
                this.updateTable();
                return true;
            } catch (Exception e)
            {
                //do nothing, it was not a valid JSON
                Console.WriteLine("Error updating Info Panel: {0}, error: {1}", json, e.Message);
                return false;
            }
        }

        private void Table_Paint(object sender, PaintEventArgs e)
        {
            //var uipColor = Color.FromArgb(250, 70, 22);
            //var uipPen = new Pen(uipColor);
            //e.Graphics.DrawRectangle(uipPen, this.Bounds);
            //ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, uipColor, ButtonBorderStyle.Solid);
        }

        public static bool getPauseStatus(int handle)
        {
            string path = GetPauseFileName(handle);
            var result = System.IO.File.Exists(path);
            return result;
        }
        public static bool getCloseStatus(int handle)
        {
            string path = GetCloseFileName(handle);
            var result = System.IO.File.Exists(path);
            return result;
        }

        private static string GetPauseFileName(int handle)
        {
            return string.Format("{0}_activity_pause_{1}", System.IO.Path.GetTempPath(), handle);
        }
        private static string GetCloseFileName(int handle)
        {
            return string.Format("{0}_activity_close_{1}", System.IO.Path.GetTempPath(), handle);
        }

        public static bool setPauseStatus(int handle, bool pause)
        {
            return setStateFile(GetPauseFileName(handle), pause);
        }

        public static bool setCloseStatus(int handle, bool pause)
        {
            return setStateFile(GetCloseFileName(handle), pause);
        }

        public static bool setStateFile(string path, bool state)
        {
            try
            {
                if (state)
                {
                    if (!System.IO.File.Exists(path))
                    {
                        var file = System.IO.File.Create(path);
                        file.Close();
                        return true;
                    }
                    return true;
                } else
                {
                    System.IO.File.Delete(path);
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception when setting State: source {0} msg {1} state {2}", e.Source, e.Message, state);
                return System.IO.File.Exists(path);
            }
        }
    }
}