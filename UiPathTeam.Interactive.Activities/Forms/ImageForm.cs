﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UiPathTeam.Interactive.Activities.Properties;

namespace UiPathTeam.Interactive.Activities.Forms
{
    public partial class ImageForm : GenericForm
    {
        public ImageForm()
        {
            InitializeComponent();
        }

        private void PictureBox_Click(object sender, EventArgs e)
        {
            this.TopMost = !this.TopMost;
        }

        public void showImage(Image image, int scale)
        {
            if (scale < 1 || scale > 100) scale = 100;
            this.Width = image.Width * scale / 100;
            this.Height = image.Height * scale / 100;
            this.pictureBox.Image = image;
        }

        private void updateSize(int scale)
        {
            if (scale < 1 || scale > 100) scale = 100;
            this.Width = pictureBox.Image.Width * scale / 100;
            this.Height = pictureBox.Image.Height * scale / 100;
        }

        public static void UpdateWindow(int handle, WindowPositions position, int scale)
        {
            if (scale > 0 && scale <= 100) NativeMethods.sendWindowsStringMessage(handle, NativeMethods.SIZE_UPDATE, scale.ToString());
            NativeMethods.sendWindowsStringMessage(handle, NativeMethods.POSITION_UPDATE, ((int)position).ToString());
        }

        protected override void WndProc(ref Message m)
        {
            try
            {
                switch (m.Msg)
                {
                    case NativeMethods.WM_COPYDATA:
                        NativeMethods.COPYDATASTRUCT mystr = new NativeMethods.COPYDATASTRUCT();
                        Type mytype = mystr.GetType();
                        mystr = (NativeMethods.COPYDATASTRUCT)m.GetLParam(mytype);

                        switch ((int)m.WParam)
                        {
                            case NativeMethods.SIZE_UPDATE:
                                int value;
                                Int32.TryParse(mystr.lpData, out value);
                                this.updateSize(value);
                                break;
                            case NativeMethods.POSITION_UPDATE:
                                int pos_value;
                                Int32.TryParse(mystr.lpData, out pos_value);
                                Console.WriteLine("position:{0}", pos_value);
                                this.SetPosition((WindowPositions)pos_value);
                                break;
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            base.WndProc(ref m);
        }
    }
}
