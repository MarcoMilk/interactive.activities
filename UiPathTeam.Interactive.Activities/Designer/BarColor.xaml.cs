﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UiPathTeam.Interactive.Activities.Properties;

namespace UiPathTeam.Interactive.Activities.Designer
{
    /// <summary>
    /// Interaction logic for BarColor.xaml
    /// </summary>
    public partial class BarColor : UserControl
    {
        public BarColor()
        {
            InitializeComponent();
            barColors.ItemsSource = LocalizedEnum<ProgressColors>.GetLocalizedValues();
            barColors.DisplayMemberPath = nameof(LocalizedEnum.Name);
            barColors.SelectedValuePath = nameof(LocalizedEnum.Value);
        }
    }
}
