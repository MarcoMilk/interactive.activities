﻿using System.Activities.Presentation.Metadata;
using System.ComponentModel;

namespace UiPathTeam.Interactive.Activities.Design
{
    public class DesignerMetadata : IRegisterMetadata
    {
        public void Register()
        {
            AttributeTableBuilder attributeTableBuilder = new AttributeTableBuilder();
            attributeTableBuilder.AddCustomAttributes(typeof(DisplayMessage), new DesignerAttribute(typeof(Designer.DisplayMessageDesigner)));
            attributeTableBuilder.AddCustomAttributes(typeof(CloseWindow), new DesignerAttribute(typeof(Designer.CloseWindowDesigner)));
            attributeTableBuilder.AddCustomAttributes(typeof(DisplayToast), new DesignerAttribute(typeof(Designer.DisplayToastDesigner)));
            attributeTableBuilder.AddCustomAttributes(typeof(ProgressBar), new DesignerAttribute(typeof(Designer.ProgressBarDesigner)));
            attributeTableBuilder.AddCustomAttributes(typeof(InfoPanel), new DesignerAttribute(typeof(Designer.InfoPanelDesigner)));
            attributeTableBuilder.AddCustomAttributes(typeof(ProgressColor), new DesignerAttribute(typeof(Designer.ProgressColorDesigner)));
            attributeTableBuilder.AddCustomAttributes(typeof(DisplayHtml), new DesignerAttribute(typeof(Designer.DisplayHtmlDesigner)));
            attributeTableBuilder.AddCustomAttributes(typeof(DisplayImage), new DesignerAttribute(typeof(Designer.DisplayImageDesigner)));
            MetadataStore.AddAttributeTable(attributeTableBuilder.CreateTable());
        }
    }
}