﻿using System;
using System.Activities;
using System.Drawing;
using UiPathTeam.Interactive.Activities.Properties;

namespace UiPathTeam.Interactive.Activities
{
    [LocalizedCategory(nameof(Resources.AttendedCategory))]
    [LocalizedDisplayName(nameof(Resources.SafeFilename))]
    [LocalizedDescription(nameof(Resources.SafeFilenameDescription))]
    public class SafeFileName : CodeActivity
    {
        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.ValueName))]
        [LocalizedDescription(nameof(Resources.ValueDescription))]
        [RequiredArgument]
        public InArgument<string> Value { get; set; }

        [LocalizedCategory(nameof(Resources.Output))]
        [LocalizedDisplayName(nameof(Resources.Filename))]
        [LocalizedDescription(nameof(Resources.FilenameDescription))]
        public OutArgument<string> Filename { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var value = Value.Get(context);
            //Clean just a filename
            foreach (var c in System.IO.Path.GetInvalidFileNameChars())
            {
                value = value.Replace(c, '_');
            }
            Filename.Set(context, value);
        }
    }
}
