﻿using System;
using System.Activities;
using System.Drawing;
using System.Threading;
using UiPathTeam.Interactive.Activities.Properties;

namespace UiPathTeam.Interactive.Activities
{
    [LocalizedCategory(nameof(Resources.AttendedCategory))]
    [LocalizedDisplayName(nameof(Resources.DisplayHtml))]
    [LocalizedDescription(nameof(Resources.DisplayHtmlDescription))]
    public sealed class DisplayHtml : NativeActivity
    {

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.TitleName))]
        [LocalizedDescription(nameof(Resources.TitleDescription))]
        public InArgument<string> Title { get; set; } = Resources.TitleDescription;

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.HtmlTextName))]
        [LocalizedDescription(nameof(Resources.HtmlTextDescription))]
        public InArgument<string> HtmlText { get; set; }


        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.EnableScrollName))]
        [LocalizedDescription(nameof(Resources.EnableScrollDescription))]
        public InArgument<Boolean> EnableScroll { get; set; } = false;
        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.CenteredName))]
        [LocalizedDescription(nameof(Resources.CenteredDescription))]
        public InArgument<Boolean> Centered { get; set; } = true;

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.Width))]
        public InArgument<int> Width { get; set; } = 200;

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.Height))]
        public InArgument<int> Height { get; set; } = 300;


        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.PositionName))]
        [LocalizedDescription(nameof(Resources.PositionDescription))]
        public WindowPositions Position { get; set; } = WindowPositions.BottomRight;

        [LocalizedCategory(nameof(Resources.Output))]
        [LocalizedDisplayName(nameof(Resources.HandleName))]
        [LocalizedDescription(nameof(Resources.HandleDescription))]
        public InOutArgument<int> Handle { get; set; }

        private Forms.HtmlForm frmHtml = new Forms.HtmlForm();

        protected override void Execute(NativeActivityContext context)
        {
            var html = HtmlText.Get(context);
            var title = Title.Get(context);
            var centered = Centered.Get(context);
            var scroll = EnableScroll.Get(context);
            var width = Width.Get(context);
            var height = Height.Get(context);

            int? handle = Handle.Get(context);

            if (handle.HasValue && handle.Value != 0)
            {
                Forms.HtmlForm.UpdateWindow(handle.Value, title, html, width, height);
                return;
            }

            frmHtml.Width = width;
            frmHtml.Height = height;
            frmHtml.Centered = centered;

            frmHtml.setScroll(scroll);

            handle = ShowForm(title, html, Position);

            Handle.Set(context, handle.Value);
            Thread viewerThread = new Thread(delegate ()
            {
                Forms.HtmlForm viewer = frmHtml;
                viewer.Show();
                System.Windows.Threading.Dispatcher.Run();
            });

            viewerThread.SetApartmentState(ApartmentState.STA); // needs to be STA or throws exception
            viewerThread.Start();
        }

        private int ShowForm(string title, string text, WindowPositions position = WindowPositions.BottomRight)
        {
            frmHtml.SetTitle(title);
            frmHtml.ShowHtml(text);
            frmHtml.SetPosition(position);
            int handle = (int)frmHtml.Handle;
            return handle;
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            //validation error
            base.CacheMetadata(metadata);
        }
    }
}
