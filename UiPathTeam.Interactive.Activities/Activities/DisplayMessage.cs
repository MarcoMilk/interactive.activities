﻿using System;
using System.Activities;
using System.Threading;
using System.Windows.Forms;
using UiPathTeam.Interactive.Activities.Forms;
using UiPathTeam.Interactive.Activities.Properties;

namespace UiPathTeam.Interactive.Activities
{
    [LocalizedCategory(nameof(Resources.AttendedCategory))]
    [LocalizedDisplayName(nameof(Resources.DisplayMessage))]
    [LocalizedDescription(nameof(Resources.DisplayMessageDescription))]
    public sealed class DisplayMessage : NativeActivity
    {

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.MessageName))]
        [LocalizedDescription(nameof(Resources.MessageDescription))]
        public InArgument<string> Message { get; set; }

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.TimerDelayName))]
        [LocalizedDescription(nameof(Resources.TimerDelayDescription))]
        public InArgument<Int32> Hide { get; set; } = 0;

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.BackColorName))]
        [LocalizedDescription(nameof(Resources.BackColorDescription))]
        public InArgument<string> BackColor { get; set; }

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.ForeColorName))]
        [LocalizedDescription(nameof(Resources.ForeColorDescription))]
        public InArgument<string> ForeColor { get; set; }

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.FontSizeName))]
        [LocalizedDescription(nameof(Resources.FontSizeDescription))]
        public InArgument<float> FontSize { get; set; }

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.PositionName))]
        [LocalizedDescription(nameof(Resources.PositionDescription))]
        public WindowPositions Position { get; set; } = WindowPositions.BottomRight;

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.PresetName))]
        [LocalizedDescription(nameof(Resources.PresetDescription))]
        public DisplayMessagePresetEnum Preset { get; set; } = DisplayMessagePresetEnum.Info;

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.OpacityName))]
        [LocalizedDescription(nameof(Resources.OpacityDescription))]
        public InArgument<double?> Opacity { get; set; } = 0.8;

        [LocalizedCategory(nameof(Resources.Output))]
        [LocalizedDisplayName(nameof(Resources.HandleName))]
        [LocalizedDescription(nameof(Resources.HandleDescription))]
        public InOutArgument<int> Handle { get; set; }

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.NewStyleName))]
        [LocalizedDescription(nameof(Resources.NewStyleDescription))]
        public InArgument<bool> NewStyle { get; set; } = true;

        private Forms.MessageForm frmStatus = new Forms.MessageForm();

        private MessageForm MessageForm { get { return frmStatus; } set { frmStatus = value; } }

        protected override void Execute(NativeActivityContext context)
        {
            MessageForm = frmStatus;

            var back = BackColor.Get(context);
            var fore = ForeColor.Get(context);

            var message = Message.Get(context);
            
            var fontSize = FontSize.Get(context);
            var newStyle = NewStyle.Get(context);

            DisplayMessagePreset preset;

            switch (Preset)
            {
                case DisplayMessagePresetEnum.Info: preset = Constants.DM_PRESET_INFO; break;
                case DisplayMessagePresetEnum.Success: preset = Constants.DM_PRESET_SUCCESS; break;
                case DisplayMessagePresetEnum.Warning: preset = Constants.DM_PRESET_WARNING; break;
                case DisplayMessagePresetEnum.Error: preset = Constants.DM_PRESET_ERROR; break;
                case DisplayMessagePresetEnum.Dark: preset = Constants.DM_PRESET_DARK; break;
                default: preset = new DisplayMessagePreset(back, fore, fontSize, Constants.DEFAULT_FONTNAME);break;
            }

            double? opacity = Opacity.Get(context);
            //Console.WriteLine("opacity received: " + opacity);

            int? handle = Handle.Get(context);
            if (handle.HasValue && handle.Value != 0)
            {
                Forms.MessageForm.UpdateWindow(handle.Value, fore, back, opacity, message);
                return;
            }

            int timeout = Hide.Get(context);
            handle = ShowForm(newStyle, opacity, preset, message, Position);

            Handle.Set(context, handle.Value);
            Thread viewerThread = new Thread(delegate ()
            {
                Forms.MessageForm viewer = frmStatus; 
                viewer.Show();

                if (timeout != 0) viewer.CloseOnTimer(timeout);
                System.Windows.Threading.Dispatcher.Run();
            });

            viewerThread.SetApartmentState(ApartmentState.STA); // needs to be STA or throws exception
            viewerThread.Start();
        }

        public int ShowForm(bool style, double? opacity, DisplayMessagePreset preset, string text = "", WindowPositions position = WindowPositions.BottomRight)
        {
            frmStatus.InitForm();
            
            frmStatus.DisplayStyle = style;

            frmStatus.SetOpacity(opacity);
            frmStatus.SetFont(preset.FontName, preset.FontSize);
            frmStatus.SetData(text, position);
            frmStatus.SetColorsFromString(preset.ForeColor, preset.BackColor);

            int handle = (int)frmStatus.Handle;
            return handle;
        }

        public void Show()
        {
            frmStatus.Show();
        }

        public void SetColors(string fore = "", string back = "")
        {
            frmStatus.SetColorsFromString(fore, back);
        }


        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            //validation error
            base.CacheMetadata(metadata);
        }
    }
}
