﻿using System.Activities;
using System.Threading;
using UiPathTeam.Interactive.Activities.Properties;
using UiPathTeam.Interactive.Activities.Forms;
using System;

namespace UiPathTeam.Interactive.Activities
{
    [LocalizedCategory(nameof(Resources.AttendedCategory))]
    [LocalizedDisplayName(nameof(Resources.ProgressBar))]
    [LocalizedDescription(nameof(Resources.ProgressBarDescription))]
    public sealed class ProgressBar : NativeActivity
    {
        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.ValueName))]
        [LocalizedDescription(nameof(Resources.ValueDescription))]
        [RequiredArgument]
        public InArgument<int> Value { get; set; } = 0;

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.MinimumName))]
        [LocalizedDescription(nameof(Resources.MinimumDescription))]
        [RequiredArgument]
        public InArgument<int> Minimum { get; set; } = 0;

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.MaximumName))]
        [LocalizedDescription(nameof(Resources.MaximumDescription))]
        [RequiredArgument]
        public InArgument<int> Maximum { get; set; } = 100;

        [LocalizedCategory(nameof(Resources.Misc))]
        [LocalizedDisplayName(nameof(Resources.HandleName))]
        [LocalizedDescription(nameof(Resources.HandleDescription))]
        public InOutArgument<int> Handle { get; set; }

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.BarColorName))]
        [LocalizedDescription(nameof(Resources.BarColorDescription))]
        public ProgressColors BarColor { get; set; }

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.PositionName))]
        [LocalizedDescription(nameof(Resources.PositionDescription))]
        public WindowPositions Position { get; set; } = WindowPositions.Center;

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.SizeName))]
        [LocalizedDescription(nameof(Resources.SizeDescription))]
        public WindowSizes Size { get; set; } = WindowSizes.Normal;

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.ShowValueName))]
        [LocalizedDescription(nameof(Resources.ShowValueDescription))]
        public bool ShowValue { get; set; } = true;

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.ValueTemplateName))]
        [LocalizedDescription(nameof(Resources.ValueTemplateDescription))]
        public InArgument<string> Template { get; set; }

        [LocalizedCategory(nameof(Resources.Visuals))]
        [LocalizedDisplayName(nameof(Resources.BarStyleName))]
        [LocalizedDescription(nameof(Resources.BarStyleDescription))]
        public System.Windows.Forms.ProgressBarStyle BarStyle { get; set; } = System.Windows.Forms.ProgressBarStyle.Continuous;
        public object progressBar { get; private set; }

        private ProgressBarForm progressBarForm = new ProgressBarForm();

        protected override void Execute(NativeActivityContext context)
        {
            int value = Value.Get(context);
            int min = Minimum.Get(context);
            int max = Maximum.Get(context);

            int? handle = Handle.Get(context);
            string tpl = Template.Get(context);
            tpl = String.IsNullOrEmpty(tpl) ? "{0}%" : tpl;

            progressBarForm.progressBar.Minimum = min;
            progressBarForm.progressBar.Maximum = max;

            if (value < progressBarForm.progressBar.Minimum) value = progressBarForm.progressBar.Minimum;
            if (value > progressBarForm.progressBar.Maximum) value = progressBarForm.progressBar.Maximum;

            if (handle.HasValue && handle.Value != 0)
            {
                ProgressBarForm.UpdateValue(handle.Value, value);
                ProgressBarForm.UpdateColor(handle.Value, BarColor);
                //ProgressBarForm.UpdateOpacity(handle.Value, opacity);
                return;
            }

            handle = (int)progressBarForm.Handle;
            Handle.Set(context, handle);

            progressBarForm.Template = tpl;
            progressBarForm.ShowValue = ShowValue;
            progressBarForm.progressBar.Style = BarStyle;
            progressBarForm.UpdateColor(BarColor);
            progressBarForm.UpdateValue(value);
            //progressBarForm.UpdateOpacity(opacity);

            Thread viewerThread = new Thread(delegate ()
            {
                ProgressBarForm viewer = progressBarForm;
                viewer.SetSize(Size);
                viewer.SetPosition(Position);
                viewer.Show();
                System.Windows.Threading.Dispatcher.Run();
            });

            viewerThread.SetApartmentState(ApartmentState.STA); // needs to be STA or throws exception
            viewerThread.Start();
        }
    }
}